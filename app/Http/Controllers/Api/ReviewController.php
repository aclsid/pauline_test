<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$review = $request->validate([
			'rating' => 'numeric',
			'language' => 'numeric',
			'lessons' => 'numeric'
		]);

		$review = (object)$review;
		$review->review = filter_var($request->input('review'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

		$reviewModel = new \App\Review();
		$review->reviewId = $reviews = $reviewModel->saveReview($review);

		return response()->json([
			'reviewId' => $review->reviewId
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$review = $request->validate([
			'rating' => 'numeric',
			'language' => 'numeric',
			'lessons' => 'numeric'
		]);

		$review = (object)$review;
		$review->review = filter_var($request->input('review'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

		$review->reviewId = (int)$id;

		$reviewModel = new \App\Review();
		$reviewModel->updateReview($review);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$reviewId = (int)$id;

		$reviewModel = new \App\Review();
		$reviewModel->deleteReview($reviewId);
	}
}
