<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function index()
	{
		$reviewModel = new \App\Review();
		$reviews = $reviewModel->getReviews();

		return view('user', array("reviews" => $reviews));
	}
}
