<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	public function getReviews()
	{
		return DB::select(" SELECT 
								`reviewId`,
								`rating`,
								`language`,
								`lessons`,
								`review` 
							FROM 
								`Review`");
	}

	public function saveReview($review)
	{
		DB::insert("INSERT INTO `Review` (
						`rating`,
						`language`,
						`lessons`,
						`review`
					) VALUES (
						:rating,
						:language,
						:lessons,
						:review
					)", array(
						'rating'=>$review->rating,
						'language'=>$review->language,
						'lessons'=>$review->lessons,
						'review'=>$review->review
					));

		return DB::getPdo()->lastInsertId();
	}

	public function updateReview($review)
	{
		DB::update("UPDATE 
						`Review` 
					SET
						`rating` = :rating,
						`language` = :language,
						`lessons` = :lessons,
						`review` = :review
					WHERE
						`reviewId` = $review->reviewId", array(
						'rating'=>$review->rating,
						'language'=>$review->language,
						'lessons'=>$review->lessons,
						'review'=>$review->review
					));

		return;
	}

	public function deleteReview($reviewId)
	{
		DB::delete("DELETE FROM
						`Review` 
					WHERE
						`reviewId` = $reviewId", array());
		
		return;
	}
}
