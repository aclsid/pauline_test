/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('teacher-reviews', require('./components/TeacherReviewsComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
    	reviews: options.reviews
    },
    methods: {
    	processForm: function() {
			let jsonFormData = Object.fromEntries(new FormData($('#formReview')[0]));

			var self = this;

			//We check to see if we are editing or creating a new review
			if ($('.btn-send-form').data('edit') == 1)
			{
				axios.put('/review/' + jsonFormData.reviewId,
					jsonFormData
				)
				.then(function (response) {
		
					$('#reviewModal').modal('hide');

					//Update the review that corresponds to the review ID
					self.reviews = self.reviews.map(item => {

						if (item.reviewId == jsonFormData.reviewId)
						{
							item.rating = jsonFormData.rating;
							item.language = jsonFormData.language;
							item.lessons = jsonFormData.lessons;
							item.review = jsonFormData.review;
						}
						else
							return item;
					});
				})
				.catch(function (error) {
					alert(error);
				});
			}
			else
			{
				axios.post('/review',
					jsonFormData
				)
				.then(function (response) {
		
					$('#reviewModal').modal('hide');

					//Get the new ID from the response
					jsonFormData.reviewId = response.data.reviewId;

					self.reviews.push(jsonFormData);
				})
				.catch(function (error) {
					alert(error);
				});
			}
    	},
    	newReview: function() {
			$('.btn-send-form').data('edit', 0);
    	}
    }
});