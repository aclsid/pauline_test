<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>User Profile</title>
		<link rel="stylesheet" type="text/css" href="/css/app.css">
	</head>
	<body>
		<div id="app" class="container-fluid">
			<div class="row">
				<div class="col"></div>
				<div id="app" class="col-6">
					<h4 class="mt-4">User Profile</h4>

					<div class="row">
						<div class="col">
							<img class="rounded-circle" src="/img/user_icon.png">
							<h5 class="d-block mb-2">John Doe</h5>
						</div>
						<div class="col">
							<label>Address</label>
							<p>Somewhere Ln, Springfield, LC</p>
		
							<label>E-mail</label>
							<p>test@review.com</p>
						</div>
					</div>

					<label>My Teacher Reviews</label>

					<table class="table">
						<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Rating</th>
							<th scope="col">Language</th>
							<th scope="col">Lessons</th>
							<th scope="col">Review</th>
							<th scope="col"></th>
						</tr>
						</thead>
						<tbody>
							<tr is="teacher-reviews" v-for="item in reviews" v-bind:review="item" v-bind:key="item.reviewId"></tr>
						</tbody>
					</table>

					<button class="btn btn-primary" v-on:click="newReview" data-toggle="modal" data-target="#reviewModal">New review</button>
				</div>
				<div class="col"></div>
			</div>

			<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="reviewModalTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="reviewModalTitle">Write review</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
			
						<div class="modal-body">
							<form id="formReview">
								<input type="hidden" name="reviewId" id="reviewId" value="">

								<div class="form-group">
									<label for="rating">Rate your teacher</label>
									<input type="text" class="form-control" id="rating" name="rating">
								</div>

								<div class="form-group">
									<label for="language">Which language did you learn?</label>
									<select class="form-control" id="language" name="language">
										<option value="1">English</option>
									</select>
								</div>

								<div class="form-group">
									<label for="lessons">How many lessons did you learn?</label>
									<input type="text" class="form-control" id="lessons" name="lessons">
								</div>

								<div class="form-group">
									<label for="review">Please write your review</label>
									<textarea class="form-control" id="review" name="review" rows="3"></textarea>
								</div>
							</form>
						</div>

						<div class="modal-footer">
							<button type="button" v-on:click="processForm" data-edit="0" class="btn btn-primary btn-send-form">Send</button>
						</div>
					</div>
				</div>
			</div>
		</div>


		<script type="text/javascript">
			var options = {reviews: {!! json_encode($reviews) !!} };
		</script>

		<script type="text/javascript" src="/js/app.js"></script>
	</body>
</html>
