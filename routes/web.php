<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Review API
Route::post('/review', 'Api\ReviewController@store');
Route::delete('/review/{id}', 'Api\ReviewController@destroy');
Route::put('/review/{id}', 'Api\ReviewController@update');

//Dashboard page
Route::get('/', 'UserController@index');
